# casify

Casify scans a static website, puts all static content in a user-specified directory, and updates and references to it. It currently only scans CSS and HTML files for references.

```
stack run public public_cassified static
```

Fair warning: it does not actually parse CSS and HTML, it just very naively looks for URLs that look like files and updates them if the file exists.
