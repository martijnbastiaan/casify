{-# LANGUAGE ImportQualifiedPost #-}

module Main where

-- base
import System.Environment (getArgs)
import Control.Monad.Extra (whenM)

-- me
import Casify.SomeFile

-- dlist
import Data.DList qualified as DList

-- pretty-show
import Text.Pretty.Simple

-- directory
import System.Directory

main :: IO ()
main = do
  cwd <- getCurrentDirectory
  [rootRelative, targetRelative, static] <- getArgs
  root <- makeAbsolute rootRelative
  target <- makeAbsolute targetRelative

  some <- walkSome root
  whenM (doesDirectoryExist target) (removeDirectoryRecursive target)
  writeSomeFiles root target ("/" <> static) some
