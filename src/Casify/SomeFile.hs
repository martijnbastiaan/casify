{-# LANGUAGE StrictData #-}
{-# LANGUAGE OverloadedLists #-}

module Casify.SomeFile where

-- base
import Control.Monad (filterM, forM_)

-- bytestring
import qualified Data.ByteString.Lazy as LByteString

-- cryptonite
import qualified Crypto.Hash as Crypto

-- directory
import System.Directory (doesFileExist, createDirectoryIfMissing)

-- extra
import Data.List.Extra (breakEnd, nubOrd)

-- filepath
import System.FilePath ((</>), takeFileName, takeDirectory)

-- Glob
import System.FilePath.Glob (glob)

-- network
import Network.URI qualified as URI
import Network.URI (URI)

-- unordered-containers
import Data.HashSet (HashSet)
import Data.HashSet qualified as HashSet
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

-- me
import Casify.ParsedFile (ParsedFile, parseFile, updateParsedFile, renderParsedFile)

type Digest = Crypto.Digest Crypto.SHA3_256

-- | List of file extensions of files considered volatile
volatileExts :: HashSet String
volatileExts = ["htm", "html"]

-- | List of file extensions of files considered volatile
parsableExts :: HashSet String
parsableExts = ["htm", "html", "css", "json"]

data SomeFile
  = VolatileFile ParsedFile
  | StaticParsedFile Digest ParsedFile
  | StaticBinaryFile Digest
  deriving (Show)

data SomeLocatedFile = SomeLocatedFile
  { _path :: FilePath
  , _someFile :: SomeFile
  }
  deriving (Show)

getExtension :: String -> Maybe String
getExtension fp =
  case breakEnd (== '.') fp of
    ("", _) -> Nothing
    (_, ext) -> Just ext

isVolatile :: String -> Bool
isVolatile s =
  case getExtension s of
    Just ext -> HashSet.member ext volatileExts
    Nothing -> False

parseSomeFiles :: FilePath -> [FilePath] -> IO [SomeLocatedFile]
parseSomeFiles root allFiles = mapM go allFiles
 where
  allFilesSet = HashSet.fromList allFiles
  nonVolatileFileSet = HashSet.filter (not . isVolatile) allFilesSet

  go :: FilePath -> IO SomeLocatedFile
  go fp =
    let
      (prefix, ext) = breakEnd (== '.') fp
      hashAction = Crypto.hashlazy <$> LByteString.readFile (root <> fp)
    in
      case prefix of
        (_:_)
          | HashSet.member ext volatileExts -> do
            parsed <- parseFile nonVolatileFileSet root fp
            pure (SomeLocatedFile fp (VolatileFile parsed))
          | HashSet.member ext parsableExts -> do
            digest <- hashAction
            parsed <- parseFile nonVolatileFileSet root fp
            pure (SomeLocatedFile fp (StaticParsedFile digest parsed))
        _ -> do
          digest <- hashAction
          pure (SomeLocatedFile fp (StaticBinaryFile digest))

mkNewFileMap :: FilePath -> [SomeLocatedFile] -> HashMap String String
mkNewFileMap static locatedFiles =
  HashMap.fromList (map fileTuple locatedFiles)
 where
  newName path digest =
    static </> (show digest <> "-" <> takeFileName path)

  fileTuple (SomeLocatedFile path someFile) =
    case someFile of
      VolatileFile _ -> (path, path)
      StaticParsedFile digest _ -> (path, newName path digest)
      StaticBinaryFile digest -> (path, newName path digest)

relativePath :: URI -> URI -> URI
relativePath from to = to{URI.uriPath=segmentsNew}
 where
  segmentsNew = foldl1 (</>) (map (const "..") (drop 1 segmentsFrom) <> segmentsTo)
  segmentsFrom = URI.pathSegments from
  segmentsTo = URI.pathSegments to

-- | Update paths and write files. Volatile files will stay where they are. Static
-- files (either parsed or binary) will be moved to a static directory.
writeSomeFiles :: FilePath -> FilePath -> FilePath -> [SomeLocatedFile] -> IO ()
writeSomeFiles root target static locatedFiles =
  forM_ locatedFiles $ \(SomeLocatedFile path someFile) -> do
    let
      newPath = newFileMap HashMap.! path
      Just newUri = URI.parseRelativeReference newPath

    createDirectoryIfMissing True (takeDirectory (target <> newPath))
    createDirectoryIfMissing True (takeDirectory (target <> path))

    case someFile of
      VolatileFile parsedFile -> do
        let updated = updateParsedFile newFileMap newUri parsedFile
        rendered <- renderParsedFile root newPath updated
        writeFile (target <> newPath) rendered
        writeFile (target <> path) rendered
      StaticParsedFile _ parsedFile -> do
        let updated = updateParsedFile newFileMap newUri parsedFile
        rendered <- renderParsedFile root path updated
        writeFile (target <> newPath) rendered
        writeFile (target <> path) rendered
      StaticBinaryFile _ -> do
        contents <- LByteString.readFile (root <> path)
        LByteString.writeFile (target <> newPath) contents
        LByteString.writeFile (target <> path) contents
 where
  newFileMap = mkNewFileMap static locatedFiles


-- | Walk a directory recursively, list all files.
walk ::
  -- | Root of static website
  FilePath ->
  -- | Set containing all existing files, given as an absolute URI. For example:
  --
  -- * /book.js
  -- * /examples/foo.html
  --
  IO [FilePath]
walk root = do
  ps1 <- glob (root </> "*")
  ps2 <- glob (root </> "**" </> "*")
  onlyFiles <- filterM doesFileExist (ps1 <> ps2)
  pure (nubOrd (map (drop (length root)) onlyFiles))

-- | Walk a directory, and parse them into 'SomeLocatedFile'
walkSome ::
  -- | Root of static website
  FilePath ->
  -- | Located parsed/hashed files
  IO [SomeLocatedFile]
walkSome root = do
  allFiles <- walk root
  parseSomeFiles root allFiles
