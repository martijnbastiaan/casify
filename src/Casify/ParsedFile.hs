{-# LANGUAGE StrictData #-}

{-# LANGUAGE RecordWildCards #-}
module Casify.ParsedFile where

-- base
import Control.Monad (forM_)
import GHC.Generics (Generic)

-- deepseq
import Control.DeepSeq (NFData, force)

-- filepath
import System.FilePath ((</>))

-- unordered-containers
import Data.HashSet (HashSet)
import Data.HashSet qualified as HashSet
import Data.HashMap.Strict (HashMap)
import Data.HashMap.Strict qualified as HashMap

-- network-uri
import Network.URI (URI)
import Network.URI qualified as URI
import System.IO (withFile, IOMode (ReadMode), hGetContents, hGetChar)

singleQuote, doubleQuote, backslash :: Char
singleQuote = '\''
doubleQuote = '"'
backslash = '\\'

data ParsedFile = ParsedFile [Reference]
  deriving (Show)

data Reference = Reference
  { _start :: Integer
    -- ^ Position in file where URI starts
  , _length :: Integer
    -- ^ Length of (original) URI in file
  , _uri :: URI
    -- ^ Parsed URI, guaranteed to be relative. I.e., no 'URI.uriScheme' or
    -- 'URI.uriAuthority' set.
  }
  deriving (Generic, Show, NFData)

parseString :: Char -> String -> Maybe String
parseString delim = go
 where
  go (c1:rest)
    | c1 == delim = Just [] -- URIs don't use escape characters (very optimistic)
    | c1 == '\n' = Nothing -- URIs don't span multiple lines
    | otherwise = fmap (c1:) (go rest)
  go _ = Nothing

parseReference :: Integer -> Char -> String -> Maybe Reference
parseReference start c s = do
  uri0 <- parseString c s
  uri1 <- if null uri0 then Nothing else Just uri0
  uri2 <- URI.parseRelativeReference uri1
  pure (Reference start (fromIntegral (length uri0)) uri2)

parseReferences :: String -> [Reference]
parseReferences = go 0
 where
  go !start (c:s)
    | c `elem` [singleQuote, doubleQuote]
    , Just ref <- parseReference (start + 1) c s
    = ref : go (start + 1) s
    | otherwise
    = go (start + 1) s
  go _ _ = []

canonicalizeReferences :: FilePath -> [Reference] -> [Reference]
canonicalizeReferences relPath = map go
 where
  go ref = ref{_uri=_uri ref `URI.relativeTo` fileUri}
  Just fileUri = URI.parseRelativeReference relPath

doesReferenceExist :: HashSet FilePath -> Reference -> Bool
doesReferenceExist existing Reference{_uri} =
  HashSet.member (URI.uriPath _uri) existing

parseFile ::
  -- | Existing paths
  HashSet FilePath ->
  -- | Root of static website
  FilePath ->
  -- | Relative path to file
  FilePath ->
  -- |
  IO ParsedFile
parseFile existing root relPath = do
  let realPath = root <> relPath
  contents <- readFile realPath
  let
    refs0 = parseReferences contents
    refs1 = canonicalizeReferences relPath refs0
    refs2 = filter (doesReferenceExist existing) refs1

  pure (ParsedFile (force refs2))

stripCommonPrefix :: Eq a => [a] -> [a] -> ([a], [a])
stripCommonPrefix (a:as) (b:bs) | a == b = stripCommonPrefix as bs
stripCommonPrefix as bs = (as, bs)

-- | Compute relative path from one absolute URI to another
relativePath :: URI -> URI -> URI
relativePath from to = to{URI.uriPath=segmentsNew2}
 where
  segmentsNew2 = URI.normalizePathSegments (foldl1 (</>) segmentsNew0)
  segmentsNew0
    -- TODO: Optimize
    | segmentsFrom == init segmentsTo = [last segmentsTo]
    | otherwise = segmentsUp <> segmentsTo

  segmentsUp = map (const "..") segmentsFrom
  segmentsFrom = init (URI.pathSegments from)
  segmentsTo = URI.pathSegments to

updateParsedFile ::
  -- | Old-to-new name map
  HashMap String String ->
  -- | Location of given parsed file
  URI ->
  -- | Parsed file to update
  ParsedFile ->
  -- | Parsed file with updated references
  ParsedFile
updateParsedFile nameMap loc (ParsedFile refs) =
  ParsedFile (map go1 refs)
 where
  go1 ref = ref{_uri=go2 (_uri ref)}
  go2 uri0 =
    let
      newPath = nameMap HashMap.! URI.uriPath uri0
      uri1 = uri0{URI.uriPath=newPath}
    in
      relativePath loc uri1

renderParsedFile :: FilePath -> FilePath -> ParsedFile -> IO String
renderParsedFile root path (ParsedFile refs) =
  withFile (root <> path) ReadMode (go 0 refs)

 where
  go _ [] handle = do
    contents <- hGetContents handle
    force contents `seq` pure contents
  go pos refs0@(Reference{..}:refs1) handle
    | pos == _start = do
      forM_ [1.._length] (\_ -> hGetChar handle)
      (show _uri <>) <$> go (pos + _length) refs1 handle
    | otherwise = do
      c <- hGetChar handle
      (c:) <$> go (pos + 1) refs0 handle
